// Main authors: Matevz Tadel & Alja Mrak-Tadel: 2006, 2007

/**************************************************************************
 * Copyright(c) 1998-2008, ALICE Experiment at CERN, all rights reserved. *
 * See http://aliceinfo.cern.ch/Offline/AliRoot/License.html for          *
 * full copyright notice.                                                 *
 **************************************************************************/

#pragma link off all functions;
#pragma link off all globals;
#pragma link off all classes;

// AliEveFileDialog
#pragma link C++ class AliEveFileDialog+;

// AliEveApplication
#pragma link C++ class AliEveApplication+;

// AliEveMainWindow
#pragma link C++ class AliEveMainWindow+;

// AliEveConfigManager
#pragma link C++ class AliEveConfigManager+;

// AliEveConfigManager
#pragma link C++ class AliEveUtil+;

// AliEveEventManager
#pragma link C++ class AliEveEventManager+;
#pragma link C++ class AliEveDataSourceOffline+;
#pragma link C++ class AliEveEventManagerEditor+;
#pragma link C++ class AliEveEventManagerWindow+;
#pragma link C++ class AliEveDataSource+;
#ifdef ZMQ
#pragma link C++ class AliEveDataSourceHLTZMQ+;
#pragma link C++ class AliEveDataSourceOnline+;
#endif

// AliEveSaveViews
#pragma link C++ class AliEveSaveViews+;

// AliEveEventSelector
#pragma link C++ class AliEveEventSelector+;
#pragma link C++ class AliEveEventSelectorWindow+;

// AliEveMacro and AliEveMacroExecutor
#pragma link C++ class AliEveMacro+;
#pragma link C++ class AliEveMacroEditor+;
#pragma link C++ class AliEveMacroExecutor+;
#pragma link C++ class AliEveMacroExecutorWindow+;

// AliEvePreferencesWindow
#pragma link C++ class AliEvePreferencesWindow+;

// Special GED editor for VizDB interaction.
#pragma link C++ class AliEveGedEditor+;
#pragma link C++ class AliEveGedNameFrame+;

// Various
#pragma link C++ class AliEveKineTools+;
#pragma link C++ class AliEveMagField+;
#pragma link C++ class AliEveVSDCreator+;
#pragma link C++ class AliEveMultiView+;

// AliEveTrack
#pragma link C++ class AliEveTrack+;
#pragma link C++ class AliEveTracklet+;

// AliEveTrackcounter
#pragma link C++ class AliEveTrackCounter+;
#pragma link C++ class AliEveTrackCounterEditor+;

// AliEveTrackFitter
#pragma link C++ class AliEveTrackFitter+;
#pragma link C++ class AliEveTrackFitterEditor+;

#pragma link C++ class AliEveCosmicRayFitter+;
#pragma link C++ class AliEveCosmicRayFitterEditor+;

// AliEveJetPlane
#pragma link C++ class AliEveJetPlane+;
#pragma link C++ class AliEveJetPlaneGL+;
#pragma link C++ class AliEveJetPlaneEditor+;
#pragma link C++ class AliEveJetPlaneEditor::StaticDataWindow+;


// AliEveCascade
#pragma link C++ class AliEveCascade+;
#pragma link C++ class AliEveCascadeEditor+;
#pragma link C++ class AliEveCascadeList+;
#pragma link C++ class AliEveCascadeListEditor+;

// AliEveV0
#pragma link C++ class AliEveV0+;
#pragma link C++ class AliEveV0List+;
#pragma link C++ class AliEveV0Editor+;
#pragma link C++ class AliEveV0ListEditor+;

// AliEveKink
#pragma link C++ class AliEveKink+;
#pragma link C++ class AliEveKinkList+;
#pragma link C++ class AliEveKinkEditor+;
#pragma link C++ class AliEveKinkListEditor+;

// AliEveHF
#pragma link C++ class AliEveHF+;
#pragma link C++ class AliEveHFList+;
#pragma link C++ class AliEveHFEditor+;
#pragma link C++ class AliEveHFListEditor+;
