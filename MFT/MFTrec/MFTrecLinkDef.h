#ifdef __CINT__
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
 
#pragma link C++ class  AliMFTReconstructor+;
#pragma link C++ class  AliMFTTrackerMU+;
#pragma link C++ class  AliMFTClusterFinder+;
#pragma link C++ class  AliMuonForwardTrack+;
#pragma link C++ class  AliMuonForwardTrackPair+;
#pragma link C++ class  AliESDEventMFT+;
#pragma link C++ class  AliMuonForwardTrackFinder+;
#pragma link C++ class  AliMFTClusterQA+;
#pragma link C++ class  AliMFTRecoParam+;

#endif
