#ifdef __CINT__
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
 
#pragma link C++ class  AliMFTConstants+;
#pragma link C++ class  AliMFTPlane+;
#pragma link C++ class  AliMFTSegmentation+;
#pragma link C++ class  AliMFTDigit+;
#pragma link C++ class  AliMFTCluster+;
#pragma link C++ class  AliMFTAnalysisTools+;

#endif
