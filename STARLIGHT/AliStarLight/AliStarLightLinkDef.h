#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ class TStarLight+;
#pragma link C++ class AliGenStarLight+;
#pragma link C++ class AliDecayerSLEvtGen+;
#pragma link C++ class AliGenSLEvtGen+;
#endif
