#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class AliEMCALTriggerDataGrid<float>+;
#pragma link C++ class AliEMCALTriggerDataGrid<double>+;
#pragma link C++ class AliEMCALTriggerDataGrid<int>+;
#pragma link C++ class AliEMCALTriggerDataGrid<char>+;

#pragma link C++ class AliEMCALTriggerRawPatch+;
#pragma link C++ class AliEMCALTriggerPatchFinder<float>+;
#pragma link C++ class AliEMCALTriggerPatchFinder<double>+;
#pragma link C++ class AliEMCALTriggerPatchFinder<int>+;
#pragma link C++ class AliEMCALTriggerAlgorithm<float>+;
#pragma link C++ class AliEMCALTriggerAlgorithm<double>+;
#pragma link C++ class AliEMCALTriggerAlgorithm<int>+;
#pragma link C++ class AliEMCALJetTriggerAlgorithm<float>+;
#pragma link C++ class AliEMCALJetTriggerAlgorithm<double>+;
#pragma link C++ class AliEMCALJetTriggerAlgorithm<int>+;
#pragma link C++ class AliEMCALGammaTriggerAlgorithm<float>+;
#pragma link C++ class AliEMCALGammaTriggerAlgorithm<double>+;
#pragma link C++ class AliEMCALGammaTriggerAlgorithm<int>+;
#pragma link C++ class AliEMCALTriggerChannelContainer+;
#pragma link C++ class AliEMCALTriggerChannelContainer::AliEMCALTriggerChannelPosition+;
#pragma link C++ class AliEMCALTriggerPatchInfo+;
#pragma link C++ class AliEMCALTriggerBitConfig+;
#pragma link C++ class AliEMCALTriggerBitConfigOld+;
#pragma link C++ class AliEMCALTriggerBitConfigNew+;

#endif
