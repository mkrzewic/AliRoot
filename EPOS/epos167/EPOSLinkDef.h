#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class TEpos+;
#pragma link C++ class AliGenEpos+;
#pragma link C++ class AliGenEposIsajetToPdgConverter+;
#endif
